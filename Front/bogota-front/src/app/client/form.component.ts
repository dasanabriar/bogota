import { Component, OnInit } from '@angular/core';
import {Client} from './client'
import {ClientService} from './client.service'
import {Router, ActivatedRoute} from '@angular/router'
import swal from 'sweetalert2'

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html'
})
export class FormComponent implements OnInit {

   client: Client = new Client()
   titulo:string = "Crear Cliente"

  constructor(private clientService: ClientService,
  private router: Router,
private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.loadClient()
  }

  loadClient(): void{
    this.activatedRoute.params.subscribe(params => {
      let id = params['id']
      if(id){
        this.clientService.getClient(id).subscribe( (client) => this.client = client)
      }
    })
  }

  create(): void {
    this.clientService.create(this.client)
      .subscribe(client => {
        this.router.navigate(['/clients'])
        swal('Nuevo client', `Client ${client.name} creado con éxito!`, 'success')
      }
      );
  }

  update():void{
    this.clientService.update(this.client)
    .subscribe( client => {
      this.router.navigate(['/clients'])
      swal('Cliente Actualizado', `Client ${client.name} actualizado con éxito!`, 'success')
    }

    )
  }

}
