package com.co.bogota.app.service.impl;

import com.co.bogota.app.models.entity.Client;
import com.co.bogota.app.models.repository.IClientRepository;
import com.co.bogota.app.service.IClientService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class ClientService implements IClientService {

    @Autowired
    private IClientRepository clientRepository;

    @Override
    public List<Client> findAll() {
        log.info("find all clients");
        return (List<Client>) clientRepository.findAll();
    }

    @Override
    public Client findById(long id) {
        return clientRepository.findById(id).get();
    }

    @Override
    public Client save(Client client) {
        return clientRepository.save(client);
    }

    @Override
    public boolean delete(Client client) {
        clientRepository.delete(client);
        return true;
    }

    @Override
    public Client update(Client client) {
        Client foundClient = findById(client.getId());
        foundClient.setName(client.getName());
        foundClient.setLastName(client.getLastName());
        foundClient.setEmail(client.getEmail());
        return save(foundClient);
    }
}
