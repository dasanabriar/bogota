package com.co.bogota.app.service.impl;

import com.co.bogota.app.models.entity.Client;
import com.co.bogota.app.models.repository.IClientRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Date;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ClientServiceTest {

    @InjectMocks
    ClientService toTest;

    @Mock
    IClientRepository clientRepository;

    @Test
    public void givenValidClient_WhenUpdate_ThenSuccess() {
        Client findClient = clientSuccess();
        findClient.setName("Mario");
        when(clientRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(findClient));
        when(clientRepository.save(Mockito.any(Client.class))).thenReturn(clientSuccess());
        Client result = toTest.update(clientSuccess());
        assertEquals("Pedro", result.getName());
    }

    private Client clientSuccess() {
        Client clientSuccess = new Client();
        clientSuccess.setId(3);
        clientSuccess.setName("Pedro");
        clientSuccess.setLastName("Pablo");
        clientSuccess.setEmail("ppabño@any.com");
        clientSuccess.setCreateDate(new Date());
        return clientSuccess;
    }

}